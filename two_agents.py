import gymnasium as gym
import numpy as np
import matplotlib.pyplot as plt
import sys
import pickle
from collections import defaultdict
from tqdm import tqdm
import text_flappy_bird_gym
class MonteCarloAgent:
    def __init__(self, env):
        self.env = env
        self.Q_values = defaultdict(lambda: np.zeros(env.action_space.n))
        self.V = None
        self.policy = None
        self.num_episodes = 0
    def play_episode(self, epsilon):
        episode = []
        state, _ = self.env.reset()
        while True:
            action = np.random.choice(np.arange(self.env.action_space.n), p=self.choose_action_probs(state, epsilon)) \
                if state in self.Q_values else self.env.action_space.sample()
            next_state, reward, done, _, _ = self.env.step(action)
            episode.append((state, action, reward))
            state = next_state
            if done:
                break
        return episode

    def choose_action_probs(self, state, epsilon):
        action_probs = np.ones(self.env.action_space.n) * epsilon / self.env.action_space.n
        best_action = np.argmax(self.Q_values[state])
        action_probs[best_action] += 1 - epsilon
        return action_probs

    def update_Q_values(self, episode, alpha, gamma):
        states, actions, rewards = zip(*episode)
        discounts = np.array([gamma ** i for i in range(len(rewards) + 1)])
        for i, state in enumerate(states):
            self.Q_values[state][actions[i]] += alpha * (self.calculate_return(rewards[i:], gamma) - self.Q_values[state][actions[i]])

    def calculate_return(self, rewards, gamma):
        return sum(r * (gamma ** i) for i, r in enumerate(rewards))

    def monte_carlo_control(self, num_episodes, alpha=0.01, gamma=1.0, epsilon_start=1.0, epsilon_decay=.99999, epsilon_min=0.05):
        print('Started Training MonteCarlo Agent')
        epsilon = epsilon_start
        self.num_episodes=num_episodes
        for episode_idx in tqdm(range(1, num_episodes + 1)):

            epsilon = max(epsilon * epsilon_decay, epsilon_min)
            episode = self.play_episode(epsilon)
            self.update_Q_values(episode, alpha, gamma)

        self.V = {k: np.max(v) for k, v in self.Q_values.items()}
        self.policy = {state: np.argmax(action_vals) for state, action_vals in self.Q_values.items()}
        print('Finished Training MonteCarlo Agent')

    def save_results(self, file_prefix="MC"):
        Q_idle = {}
        Q_flap = {}
        for key, value in self.Q_values.items():
            Q_idle[key] = value[0]
            Q_flap[key] = value[1]

        with open(f"{file_prefix}_Action_value_function_idle.pkl", 'wb') as f:
            pickle.dump(Q_idle, f)

        with open(f"{file_prefix}_Action_value_function_flap.pkl", 'wb') as f:
            pickle.dump(Q_flap, f)

        with open(f"{file_prefix}_Policy.pkl", 'wb') as f:
            pickle.dump(self.policy, f)

        with open(f"{file_prefix}_Value_function.pkl", 'wb') as f:
            pickle.dump(self.V, f)

    def plot_q_value_heatmap(self):
        V=self.V
        state_x = [key[0] for key in V.keys()]
        state_y = [key[1] for key in V.keys()]
        q_values = [val for val in V.values()]

        # Find the range for the states to set up the axes of the heatmap
        max_x = max(state_x)
        min_x = min(state_x)
        max_y = max(state_y)
        min_y = min(state_y)

        # Set up the grid for the heatmap
        grid_x, grid_y = np.meshgrid(range(min_x, max_x + 1), range(min_y, max_y + 1))

        # Initialize a grid for Q values
        q_value_grid = np.full(grid_x.shape, np.nan)

        # Populate the grid with Q values
        for x, y, q in zip(state_x, state_y, q_values):
            q_value_grid[y - min_y, x - min_x] = q  # Notice the reversal of indices to match the meshgrid

        # Plotting the heatmap
        plt.figure(figsize=(10, 8))
        c = plt.pcolormesh(grid_x, grid_y, q_value_grid, cmap='plasma', shading='auto')  # Changed color map to 'plasma'
        plt.colorbar(c, label='Q value')

        # Set the ticks for the states
        plt.xticks(ticks=np.arange(min_x, max_x+1), labels=np.arange(min_x, max_x+1))
        plt.yticks(ticks=np.arange(min_y, max_y+1), labels=np.arange(min_y, max_y+1))

        plt.xlabel('X-State')
        plt.ylabel('Y-State')
        plt.title('State Q-value Heatmap Monte Carlo Control')
        plt.grid(visible=True, color='black', linestyle='--', linewidth=0.5)  # Added grid lines
        plt.show()

    def test_policy_and_plot_scores(self,env,max_score):
            score_list = []
            for episode in range(self.num_episodes):
                obs, info = env.reset()
                while True: 
                    action = self.policy[obs]
                    obs, reward, done, _, info = env.step(action)
                    if done or info['score'] >= max_score:
                        break
                score_list.append(info['score'])

            fig, ax = plt.subplots(figsize=(12, 6))

            fig.suptitle(f'Distribution of Scores over {self.num_episodes} Games (Monte Carlo)')
            ax.hist(score_list,bins=[i for i in range(0,max_score+1,50)],color='lightblue', alpha=0.7, label='Game score')
            ax.set_xlabel('Score')
            ax.set_ylabel('Density')
            ax.legend()

            plt.grid(visible=True, color='gray', linestyle='--', linewidth=0.7)
            plt.show()

class SarsaLambdaAgent():
    def __init__(self, env, num_episodes=100000, height=15, width=20, pipe_gap=4, epsilon=0.1, step_size=0.1, discount=1.0, seed=0, lambda_=0.6):
        self.num_episodes = num_episodes
        self.height = height
        self.width = width
        self.pipe_gap = pipe_gap
        self.epsilon = epsilon
        self.step_size = step_size
        self.discount = discount
        self.seed = seed
        self.lambda_ = lambda_
        self.env = env
        self.state_map = self._create_state_map()
        self.Q_values = {}
        self.V = {}
        self.policy = {}

    def _create_state_map(self):
        screen_size = (self.width, self.height)
        x_dist_max = screen_size[0] - int(screen_size[0] * 0.3) - 1
        x_dist_min = 0
        y_dist_max = screen_size[1] - 1 - int(self.pipe_gap // 2) - 1
        y_dist_min = -y_dist_max

        all_states = [(x, y) for x in range(x_dist_min, x_dist_max + 1) for y in range(y_dist_min - 1, y_dist_max + 1)]
        state_map = {state: index for index, state in enumerate(all_states)}
        return state_map

    def _choose_action(self, state):
        """Choose an action based on epsilon-greedy policy."""
        hashed_state = self.state_map[state]  # Convert state to hashable format
        if self.rand_generator.rand() < self.epsilon:
            return self.rand_generator.randint(self.num_actions)
        else:
            return self.argmax(self.q[hashed_state, :])

    def argmax(self, q_values):
        """argmax with random tie-breaking"""
        top = float("-inf")
        ties = []
        for i in range(len(q_values)):
            if q_values[i] > top:
                top = q_values[i]
                ties = []
            if q_values[i] == top:
                ties.append(i)
        return self.rand_generator.choice(ties)

    def train(self):
        print('Started Training SARSA Agent')
        self.rand_generator = np.random.RandomState(self.seed)

        # Initialize action-value estimates and eligibility traces
        self.num_actions = 2
        self.q = np.zeros((len(self.state_map), self.num_actions))
        self.e = np.zeros((len(self.state_map), self.num_actions))  # Eligibility traces

        for episode in tqdm(range(1, self.num_episodes + 1)):
            obs, info = self.env.reset()
            self.agent_start(obs)
            while True: 
                obs, reward, done, _, info = self.env.step(self.prev_action)
                if done:
                    self.agent_end(reward)
                    break
                else:
                    self.agent_step(reward, obs)

        # Convert Q-values to dictionary format
        for state, q_values in enumerate(self.q):
            self.Q_values[state] = q_values

        # Calculate V-values and policy
        for state, q_values in self.Q_values.items():
            best_action = np.argmax(q_values)
            self.V[state] = np.max(q_values)
            self.policy[state] = best_action

        print('Finished Training SARSA Agent')

    def agent_start(self, state):
        """Starts the agent by initializing state and selecting an action."""
        action = self._choose_action(state)
        self.prev_state = state
        self.prev_action = action
        self.e *= 0  # Reset eligibility traces
        return action
    
    def agent_step(self, reward, state):
        """A step taken by the agent."""
        action = self._choose_action(state)
        # Update eligibility trace
        self.e *= self.lambda_ * self.discount
        self.e[self.state_map[self.prev_state], self.prev_action] += 1
        
        # TD error
        td_error = reward + self.discount * self.q[self.state_map[state], action] - self.q[self.state_map[self.prev_state], self.prev_action]
        # Update Q
        self.q += self.step_size * td_error * self.e
        
        self.prev_state = state
        self.prev_action = action
        return action
    
    def agent_end(self, reward):
        """Final update when the episode ends."""
        td_error = reward - self.q[self.state_map[self.prev_state], self.prev_action]
        self.e[self.state_map[self.prev_state], self.prev_action] += 1  # Ensure eligibility for the last action
        self.q[self.state_map[self.prev_state], self.prev_action] += self.step_size * td_error   # Update Q based on the last action taken

    def save_results(self, file_prefix="SARSA"):
        Q_idle = {}
        Q_flap = {}
        for key, value in self.Q_values.items():
            Q_idle[key] = value[0]
            Q_flap[key] = value[1]

        with open(f"{file_prefix}_Action_value_function_idle.pkl", 'wb') as f:
            pickle.dump(Q_idle, f)

        with open(f"{file_prefix}_Action_value_function_flap.pkl", 'wb') as f:
            pickle.dump(Q_flap, f)

        with open(f"{file_prefix}_Policy.pkl", 'wb') as f:
            pickle.dump(self.policy, f)

        with open(f"{file_prefix}_Value_function.pkl", 'wb') as f:
            pickle.dump(self.V, f)
    def plot_q_value_heatmap(agent):
        state_x = [state[0] for state in agent.state_map.keys()]
        state_y = [state[1] for state in agent.state_map.keys()]
        q_values = [agent.V[agent.state_map[state]] for state in agent.state_map.keys()]

        # Find the range for the states to set up the axes of the heatmap
        max_x = max(state_x)
        min_x = min(state_x)
        max_y = max(state_y)
        min_y = min(state_y)

        # Set up the grid for the heatmap
        grid_x, grid_y = np.meshgrid(range(min_x, max_x + 1), range(min_y, max_y + 1))

        # Initialize a grid for Q values
        q_value_grid = np.full(grid_x.shape, np.nan)

        # Populate the grid with Q values
        for x, y, q in zip(state_x, state_y, q_values):
            q_value_grid[y - min_y, x - min_x] = q  # Notice the reversal of indices to match the meshgrid

        # Plotting the heatmap
        plt.figure(figsize=(10, 8))
        c = plt.pcolormesh(grid_x, grid_y, q_value_grid, cmap='plasma', shading='auto')  # Changed color map to 'plasma'
        plt.colorbar(c, label='Q value')

        # Set the ticks for the states
        plt.xticks(ticks=np.arange(min_x, max_x+1), labels=np.arange(min_x, max_x+1))
        plt.yticks(ticks=np.arange(min_y, max_y+1), labels=np.arange(min_y, max_y+1))

        plt.xlabel('X-State')
        plt.ylabel('Y-State')
        plt.title('State Q-value Heatmap Sarsa-Lambda Control')
        plt.grid(visible=True, color='black', linestyle='--', linewidth=0.5)  # Added grid lines
        plt.show()

    def test_policy_and_plot_scores(self,env,max_score):
        score_list = []
        for episode in range(self.num_episodes):
            obs, info = env.reset()
            while True:
                #action = self.policy[self.state_map[obs]]
                action=np.argmax(self.q[self.state_map[obs],:]) 
                obs, reward, done, _, info = env.step(action)
                # If player dead or reached a score of max_score, break
                if done or info['score'] >= max_score:
                    break
            score_list.append(info['score'])

        fig, ax = plt.subplots(figsize=(12, 6))

        fig.suptitle(f'Distribution of Scores over {self.num_episodes} Games (Sarsa lambda)')
        ax.hist(score_list,bins=[i for i in range(0,max_score+1,50)],color='lightblue', alpha=0.7, label='Game score')
        ax.set_xlabel('Score')
        ax.set_ylabel('Density')
        ax.legend()

        plt.grid(visible=True, color='gray', linestyle='--', linewidth=0.5)
        plt.show()


if __name__ == "__main__":
    # Create the Flappy Bird environment
    env = gym.make('TextFlappyBird-v0', height=15, width=20, pipe_gap=4)

    # Initialize the SARSA(lambda) agent
    agent = SarsaLambdaAgent(env,num_episodes=5000)

    # Perform SARSA(lambda) training
    agent.train()

    # Save learned results
    agent.save_results(file_prefix="SARSA")

    # Plot Q-value heatmaps
    agent.plot_q_value_heatmap()

    # Test policy and plot scores distribution
    env = gym.make('TextFlappyBird-v0', height=15, width=20, pipe_gap=4)
    agent.test_policy_and_plot_scores(env)